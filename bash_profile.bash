#!/usr/bin/env bash
for xFiles in 'build.environment' 'alias' 'build.variables' 'build.maintenance'; do
source ${userDirectory}/${xFiles}
done
cd $sourceDirectory

exitOrContinue () {
read -n 1 -p $' Press [\e[0;3many key\e[0m] to continue -- [\e[1;31mEsc\e[0;37m] to exit...'
case $REPLY in $'\e') exit ;; *) echo 'continuing.' ; esac
}

installComponent () {
echo ' Creating install log.'
readarray -t fileLog <<< "$(find ~+ -type f -name \*)"
readarray -t linkLog <<< "$(find ~+ -type l -name \*)"
printf '%s\n' "${fileLog[@]##~+}" "${linkLog[@]##~+}" > ${tempDir}/${newInstall}.installed
if [[ $xbuild = /usr ]];then echo " Copying ${PWD}/* to /" ; tar cf - * | ( cd / ; sudo tar xfp - )
else echo " Copying ${PWD}${xbuild}/* to ${xbuild}" ; cd ${PWD}${xbuild} ; tar cf - * | ( cd ${xbuild} ; sudo tar xfp - ) ; cd - &>/dev/null
fi
}

rmodule () {
	[[ $1 ]]&& {
	[[ $@ == *'32-bit'* ]]&& local logInstallDirectory=${logInstallDirectory/64/}

	[[ $PWD != $parentDirectory ]]&& { removeModule=(${removeModule/$logInstallDirectory/$PWD}) ; }
	
	local removeModule=(${logInstallDirectory}/${1}-*)
	}

	[[ -e $removeModule ]]&& { readarray -t installedList < $removeModule

		[[ $backup || $@ == *'backup'* ]]&& { local stripDirectory=${removeModule##*/}
		sudo tar --zstd -cvf ${tempDir}/${stripDirectory%installed}tar.zst -T ${removeModule}
		echo -e " Backup created: ${tempDir}/\e[1;34m${stripDirectory%installed}\e[0mtar.zst"
		[[ $mvBackup ]]&& mv ${tempDir}/${stripDirectory%installed}tar.zst ~/xbackup${libdirsuffix}${installSuffix}
		}

	for((i=0;i<${#installedList[*]};i+=2000)); do sudo rm -vf ${installedList[@]:i:2000}; done || :
	mv -v $removeModule ${removeModule//installed/removed}
	}
unset removeModule
}

recover () {
	installArchive () {
	cd ${logInstallDirectory} 

	local installedModule=(${logInstallDirectory}/${1%%-*}-*) ; 

		[[ -e "${installedModule[0]}" ]] && { echo " Install log detected. Removing previous install."
		local stripDirectory=${installedModule[0]##*/}
		rmodule "${stripDirectory[0]%%-*}"
		}
		
 	cd - &>/dev/null

	local newInstall=${archive%.tar.zst}
	mkdir -p $tempDir/${archive%.tar.zst}_Archive
	tar --zstd -xf ${archive} --directory=${tempDir}/${archive%.tar.zst}_Archive
	cd $tempDir/${archive%.tar.zst}_Archive
	installComponent
	mv ${tempDir}/*.installed ${logInstallDirectory} &>/dev/null
	cd - &>/dev/null
	sudo rm -r $tempDir/${archive%.tar.zst}_Archive
	}

for zstDirectory in $PWD $tempDir ~/xbackup${libdirsuffix}${installSuffix}; do
cd $zstDirectory
local archive=(*.tar.zst)
[[ $archive != '*.tar.zst' ]]&& { archive=("${@}") ; break ; }
done

	if [[ ${archive[@]} = '*.tar.zst' ]];then echo " No $archive archives found." ; return
	elif [[ ${#archive[@]} -gt 1 ]];then local PS3="  [\e[1;31mCtrl\e[0;37m]+[\e[1;31mc\e[0;37m] to exit or select number: "
	echo " Multiple archives detected."
		select logName in "${archive[@]}" ; do
		echo -e " $REPLY selected \e[1;34m${logName}\e[0m"
		archive="$logName"
		installArchive $archive ; return
		done
	else installArchive "$1"
	fi
}

skip () (
cp -Pau ${userDirectory}/module_progress/{build,skipped}.modules $tempDir
cd $tempDir ; head -n 1 build.modules >> skipped.modules
skipNewBuild=$(tail -n +2 build.modules) ; echo "$skipNewBuild" > build.modules ; unset skipNewBuild ; cd - &>/dev/null
)

saveProgress () {
rsync -au --inplace --no-whole-file --no-compress ${tempDir}/{build,built,skipped,updated}.modules ${userDirectory}/module_progress/
mv ${tempDir}/*.installed ${logInstallDirectory} &>/dev/null
echo " Progress saved to ${userDirectory}/module_progress"
}

exitOnError () {
if [[ ${doNotHaltOnErrors} ]];then echo ' Ignoring error.'
else [[ -d ${tempDir}/${gitDir}/${buildSubDirectory} ]]&& { echo -e " \e[1;31mBuilding ${gitDir} unfinished.\e[37m\n$(<$(ls -c1 ${tempDir}/${gitDir}/${buildSubDirectory}/{config,build,install}_error.log 2> /dev/null | head -1))" ; }
	if [[ $skipOn && ! -z $(grep '[^[:space:]]' ${tempDir}/build.modules) ]];then
	echo " Skipping ${gitDir}." #; skip && build && saveProgress
	else saveProgress ; exit 1
	fi
fi
}

resetHistoryBuildExclude () {
cd ${userDirectory}/module_progress ; >skipped.modules ; >built.modules ; cd - &>/dev/null
grep -wv "$(grep -v "^#" ${userDirectory}/exclude.list)" ${userDirectory}/build.order > ${userDirectory}/module_progress/build.modules
}

editTestsDirectories () {
testsDirectories=';s@\(.*SUBDIRS.*\)[[:space:]]\?\<tests\>\(.*\)@\1\2@g;/\<tests\>[[:space:]]*\?\\$/d'
}

editMake () {
echo -ne ".\e[1;31m"
\time -f "%es" find . -name Makefile.am -type f -execdir sed -i "
s@\(.*SUBDIRS.*\)[[:space:]]\?\<nls\>\(.*\)@\1\2@g;/\<nls\>[[:space:]]*\?\\$/d
s@\(.*SUBDIRS.*\)[[:space:]]\?\<specs\>\(.*\)@\1\2@g;/\<specs\>[[:space:]]\?\\$/d
s@\(.*SUBDIRS.*\)[[:space:]]\?\<docs\?\>\(.*\)@\1\2@g;/\<docs\?\>[[:space:]]*\?\\$/d
s@\(.*SUBDIRS.*\)[[:space:]]\?\<man\>\(.*\)@\1\2@g;/\<man\>[[:space:]]*\?\\$/d
s@\(.*DOC_SUBDIR.*\)[[:space:]]\?\<doc\>\(.*\)@\1\2@g
s@\(.*SUBDIRS.*\)[[:space:]]\?\<examples\>\(.*\)@\1\2@g
s@\(.*SUBDIRS.*\)[[:space:]]\?\<demos\>\(.*\)@\1\2@g;/\<demos\>[[:space:]]*\?\\$/d
s@\(.*SUBDIRS.*\)[[:space:]]\?\<test\>\(.*\)@\1\2@g;/\<test\>[[:space:]]*\?\\$/d${testsDirectories}" {} +
echo -ne "\e[1;37m"
}

stitch () {
for cloth in $patchList ; do 

if [[ -e $cloth ]];then
patchPath="$patchFile"
else patchPath=${userDirectory}/module_patch/${cloth} ; echo " $patchFile does not exist. Check spelling."
fi

case ${cloth} in
*.diff|*.patch) patch -p1 --verbose ${reversePatchOn} < $patchPath || echo "$cloth" >> ${tempDir}/patch.fail ;;
*.tar.?z*) patch -p1 ${reversePatchOn} <<< $(tar xvf $patchPath) || echo "$cloth" >> ${tempDir}/patch.fail ;;
*.gz) patch -p1 ${reversePatchOn} <<< $(zcat $patchPath) || echo "$cloth" >> ${tempDir}/patch.fail ;;
*.xz) patch -p1 ${reversePatchOn} <<< $(xz -dcf $patchPath) || echo "$cloth" >> ${tempDir}/patch.fail ;;
*.tar.zst) patch -p1 ${reversePatchOn} <<< $(tar --zstd -xf $patchPath) || echo "$cloth" >> ${tempDir}/patch.fail ;;
*.zst) patch -p1 ${reversePatchOn} <<< $(zstd -df $patchPath) || echo "$cloth" >> ${tempDir}/patch.fail ;;
*) echo -e "No case made for \e[1;34m${cloth}\e[0;37m." ;;
esac done
}

extractArchive () {
[[ $1 ]]&& compressedArchive=$1
case ${compressedArchive} in
*.tar.?z*) tar xvf ${compressedArchive} --directory=${tempDir} ;;
*.gz) gzip -dfk ${compressedArchive} > ${tempDir} ;;
*.xz) xz -dcf ${compressedArchive} > ${tempDir} ;;
*.tar.zst) tar --zstd -xf ${compressedArchive} --directory=${tempDir} ;;
*.zst) zstd -df ${compressedArchive} --output-dir-mirror ${tempDir} ;;
esac
}

getAndCropBeforeIndexNumber () {
for indexNumber in "${!componentDirectory[@]}" ; do [[ ${componentDirectory[indexNumber]} =~ $fromComponent$ ]]&& break ; done
componentDirectory=("${componentDirectory[@]:${indexNumber}}")
}

getIndexNumberToAndCrop () {
for (( indexNumber=${#componentDirectory[@]}-1 ; indexNumber>=0 ; indexNumber-- )); do [[ ${componentDirectory[indexNumber]} =~ $toComponent$ ]]&& break ; done
componentDirectory=("${componentDirectory[@]:0:$(($indexNumber+1))}")
}

updateStatus () {
echo -en "\e[1A" ; echo -e "\e[0K\r \e[1;34m${ifUpdated:-\e[1;30m  ${updateDir} ${gitState}\xE2\x97\x80}${ifUpdated:+ \e[0;36m\xE2\x97\x80}\e[0m"
}

update () {
for updateDir in "${updateList[@]}" ; do
cd $updateDir &>/dev/null

if [[ -d .git ]];then gitState='... ' ; updateStatus
 
	[[ $updateDefaultBranch ]]&& git remote set-head origin --auto &>/dev/null

	[[ $listGitURL ]]&& ifUpdated=$(git remote get-url origin) && echo "$ifUpdated" >> ${tempDir}/url.modules && ifUpdated="\e[0;36m\xe2\x96\xb6\e[1;37m $updateDir - $ifUpdated)"

	[[ $branchMain ]]&& git checkout origin/main
	[[ $branchMaster ]]&& git checkout origin/master
	
	[[ $updateRepository || -z $updateRepository && -z $listGitURL ]]&& {
		if git symbolic-ref HEAD &>/dev/null ;then proceed='yes'

		else ifUpdated="\e[0;36m\xe2\x96\xb6\e[1;31m $updateDir No update due to not being on a branch. Press \e[0many key\e[1;31m to checkout default branch. \e[0m[Esc]\e[1;31m to continue."
		updateStatus ; unset ifUpdated
		read -n 1 ; case $REPLY in $'\e') continue ;; *) git checkout $(git rev-parse --abbrev-ref @) &>/dev/null ; updateStatus ; unset gitState ; esac
		fi

	[[ $proceed ]]&& { git fetch --depth 1 &>/dev/null
	
		[[ $unshallow ]]&& { git fetch --unshallow &>/dev/null ; ${singleComponentBuild:+unset unshallow} ; }

	currentCheckout=$(git rev-parse --abbrev-ref --symbolic-full-name @{u})

		if [[ $(git rev-list @...${currentCheckout} --count) != 0 ]];then
		[[ -z $gitmerge && -z $gitreset ]]&& git rebase &>/dev/null ||
		[[ -z $gitreset ]]&& git merge ${currentCheckout} &>/dev/null || git reset --hard ${currentCheckout} &>/dev/null
		updatedList+=($updateDir) ; ifUpdated="\e[0;36m\xe2\x96\xb6\e[1;34m $updateDir $(git --no-pager log --oneline -n1)"
		else unset gitState
		fi
	unset proceed ; }
	}

else gitState='.git not detected. '
fi

updateStatus ; echo ''
unset ifUpdated
cd - &>/dev/null
done

printf '%s\n' "${updatedList[@]}" > ${tempDir}/updated.modules

echo -e "${updatedList:+\n \e[1;36mModules updated:\n \e[1;34m${updatedList[@]/%/'\n'}\e[37m}"

if [[ $buildUpdatedModulesOnly ]];then componentDirectory=("${updatedList[@]}") ; [[ ${componentDirectory[@]} ]]|| echo ' No changes detected.'
else fromComponent="${updatedList[0]}" ; toComponent="${updatedList[@]: -1}"
[[ $fromComponent ]]&& getAndCropBeforeIndexNumber
[[ $toComponent ]]&& getIndexNumberToAndCrop
fi
}

writeToDisk () {
echo ' Ctrl-c caught.'
saveProgress ; exit 1
}

build () (
SECONDS='0'
trap 'writeToDisk' INT
[[ ! -d ${tempDir} ]]&& { mkdir -p ${tempDir} ; }
rsync -rau --inplace --no-whole-file --no-compress ${userDirectory}/module_cfg ${tempDir}/
cfgList=(${tempDir}/module_cfg/*)
readarray -t buildList < ${userDirectory}/build.order
[[ -s ${userDirectory}/update_exclusion.modules ]]&& { readarray -t openNoUpdateList < ${userDirectory}/update_exclusion.modules ; }
declare -A version openCfg
printf -v dash '%*s' "$COLUMNS" ; line=${dash// /-}
[[ -e "${tempDir}/patch.fail" ]]&& sudo unlink ${tempDir}/patch.fail

for option in "$@" ; do case $option in
help) echo 'todo' ; exit 1 ;;
--*) [[ -z $manualFlag ]]&& { echo ' Appending to configureFlags (global config or configureFlagsDefault):' ; }
echo -e "    \e[1;34m${option}\e[0;37m" ; manualFlag+="$option " ;;
*=*) envVariable+="$option " ;;
disablebuilddir|nobuildDir|nobuilddir|builddiroff) unset buildDir ; echo ' Build directory disabled.' ;;
*.diff|*.patch|*.tar.?z*|*.[gx]z|*.tar.zst|*.zst)
	if [[ -e $option ]];then :
	elif [[ -e ${userDirectory}/module_patch/$option ]]; then option="${userDirectory}/module_patch/$option"
	fi
	
	if [[ $option == *'patch'* || $option == *'diff'* ]]
	then patchFile="${patchFile:+$patchFile$'\n'}${option}"
	else compressedArchive=$option ; extractArchive ; cd ${tempDir}/${option/[![:alpha:]]*}*/
	fi ;;
patchoff|[pP]|po) patchingOff='yes' ; echo ' Auto-patching off.' ; noClean='yes' ;;
createpatch|patch) declare {createPatch,noCleanBuildDir}='yes' ; [[ $2 ]]&& patchName="$2" ;;
reverse) reversePatchOn='-R --verbose' ; echo ' Reverse patch on.' ;;
restart|refresh|renew|re|[rR]) re ; updateNow='yes' ; echo ' build.modules (resume list) refreshed for new build.' ;;
update|updated|[uU]|up) updateModules='yes' ; lastUpdate=$(($(date +%s) - $(stat -c %Y ${userDirectory}/module_progress/updated.modules))) ; echo ' Update modules on.' ;;
repo|repository) updateRepository='yes' ; echo 'Updating repository after module url "list" creation or git remote "url" change.' ;;
now) updateNow='yes' ; echo ' Override update lock timer on.' ;;
only) updateOnly='yes' ; echo ' Updating modules only.' ;;
branch|ub) updateDefaultBranch='yes' ; echo ' Update default branch.' ;;
list|repolist) listGitURL='yes' ; declare update{Modules,Now,Only}='yes' ; unlink ${tempDir}/url.modules ; echo " Copying build list urls to ${tempDir}/url.modules" ;;
buildonly|noinstall) buildOnly='yes' ; echo ' Building only.' ;;
destdironly) destdirOnly='yes' ; echo ' Install to DESTDIR only.' ;;
test|dryrun) removePkgNo='yes' ; destdirOnly='yes' ;;
changes|updated) buildUpdatedModulesOnly='yes' ; echo ' Building changes only since update.' ;;
merge) gitmerge='yes' ; echo ' Merging remote and local files' ;;
gitreset|resethard) gitreset='yes' ; echo -e " \e[1;31mWarning\e[0;37m--matching remote repository by deleting any local files." ; exitOrContinue ;;
main|checkoutmain) branchMain='yes' ;;
master|checkoutmaster) branchMaster='yes' ;;
skip|[sS]) skipOn='yes' ; echo ' Auto-skipping on.' ;;
keepEnv|preserveDefaultEnv|[eE]) keepEnvOn='yes' ; echo ' Preserving environment on (overrides option appendEnv--appending custom environment variables).' ;;
appendEnv|ae) appendEnvOn='yes' ; echo ' Appending custom environment variables instead of overwriting default environment.' ;;
keepCfg|[gG]) keepGlobalCfgOn='yes' ; echo ' Ignore custom *.cfg. Preserve global config instead of appending/overwriting with custom *.cfg.' ;;
overwriteCfg|[oO]) overwriteCfgOn='yes' ; echo ' Overwrite global config instead of appending (default behaviour) with custom *.cfg.' ;;
syslocaloff|sysoff|so) sysLocalOff='yes' ; echo ' Disabling sysconfdir and localstatedir variables from configureFlags.' ;;
*.cfg?*) openCfg[0,1]=$option ; echo " Using config ${option}." ;;
editmake|editMake|em) editMakeOn='yes' ; echo ' Editing Makefile.am.' ;;
withnotests|testsoff|ts) disableTestsSubdirectories='yes' ; echo ' Disable build of "tests" subdirectories.';;
resume|cleanOff|noclean|rb) noCleanBuildDir='yes' ; echo ' Not deleting built code.' ;;
force|fo) doNotHaltOnErrors='-k' ; CFLAGS="${CFLAGS} -Wno-error" ; echo ' Ignore build errors.' ;;
standard|standardcfg|forcestandard) standard='yes' ; echo ' Forcing standard configure.' ;;
local|keeplocal) localDir='yes' ; echo ' Keeping local directory.' ;;
withautotools|forceautotools) forceAutotools='yes' ;;
autogensub) autogensub='yes' ; echo " autogen.sh run from $buildSubDirectory" ;;
autogensource) autogensource='yes' ; echo " autogen.sh run from $gitDir" ;;
mallocOff|mallocoff|mo) mallocOff='yes' ; echo ' Disabling malloc check during configure.' ;;
debug) CFLAGS="${CFLAGS} -g -Wall" ; echo ' Debug on.' ;;
strip|resize|shrink) stripDebugSymbols='yes' ; echo ' Remove debug symbols on.' ;;
verbose|v) verbose='V=1'; echo ' Verbose compiling.' ;;
tag|tagged|stable) useReferenceVersion='yes' ; echo ' Build tag or hash referenced to in component.version file.' ;;
version|git|checkout|revision|hash) version='yes' ;;
revert) revert='yes' ;;
unshallow) unshallow='yes' ; echo ' Download git history enabled.' ;;
copygit|withgit) copyGit='yes' ;;
backup) backup='yes' ;;
movebackup|savebackup|save) declare {mvBackup,backup}='yes' ;;
previous|restore|old) previous='yes' ; echo ' Build commit from previous install--on.' ;;
keepinstall|keepkg|removepkgno) removePkgNo='yes'; echo ' Keeping previously installed packages on.' ;;
32-bit|32bit|32) 32bitCompileOn ;;
after|a) afterComponent='yes' ; optionDividerOn='yes' ;;
before|b) beforeComponent='yes' ; optionDividerOn='yes' ;;
from|f) fromComponent='yes' ; optionDividerOn='yes' ;;
to|t) toComponent='yes' ; optionDividerOn='yes' ;;
*)
[[ -f $option && -z $customList ]] && { readarray -t buildList <${option} ; customList=${option} ; echo -e " Build list \e[1;34m${option}\e[0;37m selected." ; }

[[ $PWD = $parentDirectory && -z $ifOptionExistsInBuildOrder ]]&& for moduleNumber in ${!buildList[@]}
do [[ ${buildList[moduleNumber]} =~ $option$ ]]&& ifOptionExistsInBuildOrder=${buildList[moduleNumber]} && break
done

if [[ $version = 'yes' ]];then version=$option ; echo -e " Build git version \e[1;34m${option}\e[0;37m."
elif [[ $revert = 'yes' ]];then revert=$option ; echo -e " Reverting \e[1;34m${option}\e[0;37m commit(s)."
elif [[ $ifOptionExistsInBuildOrder ]];then
	if [[ $optionDividerOn ]];then optionDividerOn=()
	afterComponent="${afterComponent/yes/$option}" ; fromComponent="${fromComponent/yes/$option}" ; fromComponent="${fromComponent:-$afterComponent}"
	beforeComponent="${beforeComponent/yes/$option}" ; toComponent="${toComponent/yes/$option}" ; toComponent="${toComponent:-$beforeComponent}"
	else cd ${ifOptionExistsInBuildOrder} ;fi
elif [[ -d $option ]];then [[ -e ${userDirectory}/module_plugin/build.${option} ]]&& source ${userDirectory}/module_plugin/build.${option} && exit 1 || cd $option
fi;;
esac done

cp -Pau ${userDirectory}/module_progress/{build,built,skipped,updated,url}.modules ${tempDir} &>/dev/null

if [[ $PWD = $parentDirectory ]];then readarray -t componentDirectory < ${tempDir}/build.modules

	if [[ -z $customList ]];then echo -n " Building/resuming from ${userDirectory}/module_progress/build.modules."
	elif [[ -e $customList ]];then

		[[ ${buildList[@]} == ${componentDirectory[-1]} ]]|| { echo -en " (Over)writing ${tempDir}/build.modules (resume list) with new build list \e[1;34m${customList}\e[0;37m."
		unset componentDirectory ; componentDirectory=(${buildList[@]}) ; >${tempDir}/skipped.modules ; >${tempDir}/built.modules ; }

	elif [[ -e ${userDirectory}/module_list/${customList} ]];then readarray -t openCustomList < ${userDirectory}/module_list/${customList}

		if [[ ${componentDirectory[@]} = ${openCustomList[@]: -${#componentDirectory[@]}} ]];then
		echo -en " Resuming from the remainder of \e[1;34m$customList\e[0;37m saved in ${userDirectory}/module_progress/build.modules."
		else echo -en " (Over)writing ${tempDir}/build.modules (resume list) with new build list \e[1;34m${customList}\e[0;37m."
		unset componentDirectory ; componentDirectory=(${openCustomList[@]}) ; >${tempDir}/skipped.modules ; >${tempDir}/built.modules
		fi
	else echo -e " The custom build list or component or directory \e[1;34m$customList\e[0;37m does not exist--check spelling." ; exit 1
	fi

[[ $fromComponent ]]&& getAndCropBeforeIndexNumber
[[ $toComponent ]]&& getIndexNumberToAndCrop
[[ $afterComponent ]]&& { componentDirectory=("${componentDirectory[@]:1}") ; fromComponent="${fromComponent##$afterComponent}" ; }
[[ $beforeComponent ]]&& { unset 'componentDirectory[-1]' ; toComponent="${toComponent##$beforeComponent}" ; }

echo -en "${fromComponent:+..and starting from component \e[1;34m$fromComponent\e[0;37m.}"
echo -en "${toComponent:+..and ending at component \e[1;34m$toComponent\e[0;37m.}"
echo -en "${afterComponent:+..and starting after component \e[1;34m$afterComponent\e[0;37m.}"
echo -e "${beforeComponent:+..and ending before component \e[1;34m$beforeComponent\e[0;37m.}"

if [[ $updateModules ]];then printf ' Last update elapsed time: \e[0;36m%d seconds\n\e[1;30m' $(($lastUpdate))
if [[ $updateNow || $lastUpdate -ge $updateTimer ]];then
updateList=("${componentDirectory[@]}")
echo -e "${openNoUpdateList:+\n Update exclusion list:\n \e[1;34m$openNoUpdateList\e[37m\n\n}"
for updateIndex in "${!updateList[@]}" ; do [[ ${duplicateIndex[@]} == *"${updateList[updateIndex]}"* ]]&& { unset updateList[updateIndex] ; }
duplicateIndex+=( ${updateList[updateIndex]} )
for excludeIndex in "${!openNoUpdateList[@]}" ; do [[ ${updateList[updateIndex]} == *"${openNoUpdateList[excludeIndex]}"* ]]&& unset updateList[updateIndex] ;done ;done
update
else timeUnlock=$(($updateTimer - $lastUpdate)) ; printf ' \e[0;36m%d minutes %d seconds \e[0;37mtill update lock expires.\n' $(($timeUnlock%3600/60)) \ $(($timeUnlock%60))
fi fi

printf '%s\n' "${componentDirectory[@]}" > ${tempDir}/build.modules

[[ -s $tempDir/built.modules ]]&& readarray -t loadBuiltModules < ${tempDir}/built.modules
[[ -s $tempDir/skipped.modules ]]&& readarray -t loadSkippedModules < ${tempDir}/skipped.modules
progress=("${#componentDirectory[@]}" "${#loadBuiltModules[@]}" "${#loadSkippedModules[@]}")

else

parentDirectory="${ifOptionExistsInBuildOrder:+${PWD%/$ifOptionExistsInBuildOrder}}" ; parentDirectory="${parentDirectory:-${PWD%/*}}"

[[ ${parentDirectory:0:${#sourceDirectory}} != $sourceDirectory ]]&& { [[ ! -d .git && -z "$compressedArchive" ]]&& {
echo -e " .git not detected while out of source directory (${sourceDirectory})."
exitOrContinue ; } ; }

componentDirectory="$ifOptionExistsInBuildOrder" ; componentDirectory="${componentDirectory:-${PWD##*/}}"
singleComponentBuild='yes' ; progress=('1' '0' '0') ; echo ' Single component build on.'
[[ $updateModules ]]&& { updateList="$componentDirectory" ; update ; }
fi

[[ $updateOnly ]]&& { saveProgress ; exit 1 ; }

for gitDir in "${componentDirectory[@]}" ; do
originalName="${gitDir##*/}"

cfgName="${originalName}.cfg"
mendName="${originalName}.mend"
envName="${originalName}.env"
preperationBeforeConfigure="${originalName}.prep"
preInstall="${originalName}.pre"
postInstall="${originalName}.post"

ot=$((${progress[0]} + ${progress[1]} + ${progress[2]}))

t=$((${progress[0]} + ${progress[1]}))

remaining=$(($t - ${progress[1]}))
current=$(($t - $remaining + 1 ))

originalCurrent=$(($ot -$remaining + 1))

powerOfTen=$((1$hundred * 1$significantFigure))
percent=$((${progress[0]} * $powerOfTen / $t))
percent=$(($powerOfTen - $percent))

lengthOfValue=${#percent}

zeroesToAdd="$((${#powerOfTen} - $lengthOfValue))"

powerOfTen="${hundred}${significantFigure}"
powerOfTen="${powerOfTen:0:$zeroesToAdd}"
percent="${powerOfTen}${percent}"

percent="${percent:0: -${#significantFigure}}.${percent: -${#significantFigure}}"
percent="${percent#${powerOfTen}}%"
percent="${percent%[1-9]}"

echo -e "\e[1;37m"

[[ -d ${tempDir}/${gitDir} && -z $noCleanBuildDir && -z $compressedArchive ]]&& { echo " Deleting ${tempDir}/${gitDir}." ; sudo rm -rf ${tempDir}/${gitDir}/ ; }

if [[ $compressedArchive ]];then echo " Compressed archive extracted to ${tempDir}/${gitDir}" ; else

	echo " Creating directory ${tempDir}/${gitDir}." && mkdir -p ${tempDir}/${gitDir}
	echo -ne " Copying ${parentDirectory}/${gitDir}/* to ${tempDir}/${gitDir}...\e[1;31m" ;

	[[ -e ${userDirectory}/module_version/${originalName}.version1 && -z $(grep $gitDir ${tempDir}/{built,skipped}.modules) && -z $singleComponentBuild && -z $version ]]&& {
	version=$(<${userDirectory}/module_version/${originalName}.version1) ; version[0,1]=1 ; }

	if [[ $version || $useReferenceVersion || $revert || $previous || $createPatch || $copyGit ]];then
	\time -f "%es" rsync -rau --inplace --no-whole-file --no-compress ${parentDirectory}/${gitDir}/* ${parentDirectory}/${gitDir}/.git ${tempDir}/${gitDir}

	[[ -e ${userDirectory}/module_version/${originalName}.version && $useReferenceVersion && -z $version ]]&& version=$(<${userDirectory}/module_version/${originalName}.version)

	[[ $previous ]]&& { version=(${logRemoveDirectory}/${originalName}${version[0,1]}-*-${ARCH}-*.removed) ; version=${version#*-} ; version=${version%%-*} ; }

	[[ $version ]]&& git -C ${tempDir}/${gitDir} checkout $version
	[[ $previous ]]&& version[0,0]="${originalName}"
	[[ $useReferenceVersion ]]&& version[0,0]=''
	echo "${version[0,0]}"
	
	[[ $revert ]]&& {
	[[ $unshallow ]]&& git -C ${tempDir}/${gitDir} fetch --unshallow
	git -C ${tempDir}/${gitDir} reset --hard @~${revert} && echo "reverted by ${revert}"
	sleep 3
	}
	else cd ${parentDirectory}/${gitDir} ; \time -f "%es" rsync -raW --no-compress * ${tempDir}/${gitDir}
	fi
fi

echo -e "\e[1;37m"
cd ${tempDir}/${gitDir}

[[ $createPatch ]]&& { [[ -d .git ]]&& git clean -dxf

readarray -t newPatch <<< "$(diff -Naur --exclude='.*' ${parentDirectory}/${gitDir} ${tempDir}/${gitDir})"

[[ -z $newPatch ]]&& echo -e " First stage copy complete. No changes detected between ${parentDirectory}/${gitDir} and ${tempDir}/${gitDir}.\n Edit ${tempDir}/${gitDir} and then rerun 'build ${originalName} createpatch'." && exit 1

readarray -t newPatch <<< $(printf '%s\n' "${newPatch[@]/${parentDirectory}\/${gitDir}/a}")
readarray -t newPatch <<< $(printf '%s\n' "${newPatch[@]/${tempDir}\/${gitDir}/b}")

[[ -z $patchName ]]&& patchName="$originalName" ; printf '%s\n' "${newPatch[@]}" > ${tempDir}/${patchName}.patch

cp ${tempDir}/${patchName}.patch ${userDirectory}/module_patch
echo "${patchName}.patch" >> ${userDirectory}/module_mend/${originalName}.mend
echo " Copied ${tempDir}/${patchName}.patch to ${userDirectory}/module_patch and added ${patchName}.patch to ${userDirectory}/module_mend/${originalName}.mend" ; exit 1 ; }

echo -e "\e[1;37m"

[[ -z $removePkgNo ]]&& {
removeModule=(${logInstallDirectory}/${originalName}${version[0,0]}*${ARCH}-*.installed)
[[ -e $removeModule ]]&& { mv -v ${logRemoveDirectory}/${originalName}*${ARCH}-*.removed ${logArchiveDirectory}/ ; rmodule ; }
}

[[ $patchFile ]]&& { echo -e " Using patch(es) specified on command line:\n \e[1;34m$patchFile\e[37m."

patchList="$patchFile" ; $patchPath ; }

[[ -e ${userDirectory}/module_mend/${mendName} && -z $patchingOff ]]&& { echo -e " Using patch(es) found in ${userDirectory}/module_mend/\e[1;34m$mendName\e[37m."
patchList=$(<${userDirectory}/module_mend/${mendName}) ; stitch ; }

(
if [[ $editMakeOn ]];then
echo -n " Editing Makefile.am files in ${tempDir}/${gitDir}.."
[[ $disableTestsSubdirectories ]]&& { echo -n ".disabling tests directories.." && editTestsDirectories ; }
editMake
fi
)

[[ $mallocOff ]]&& { newConfigureAC=$(grep -wv 'XORG_CHECK_MALLOC_ZERO' configure.ac) && echo "$newConfigureAC" > configure.ac ; find . -name Makefile.am -type f -execdir sed -i 's_@MALLOC\_ZERO\_CFLAGS@__g' '{}' + ; }

if [[ -z $keepGlobalCfgOn ]];then
	if [[ ${openCfg[0,1]} ]];then openCfg[0,0]=$(<${openCfg[0,1]})
	elif [[ "${cfgList[@]}" =~ ${cfgName}2.me && -z $forceAutotools ]] && [[ $singleComponentBuild || ${loadBuiltModules[@]} =~ "${gitDir}" ]];then
	openCfg[0,0]=$(<${tempDir}/module_cfg/${cfgName}2.me) ; openCfg[0,1]="${cfgName}2.me" ; echo ' meson config 2 found'
	elif [[ "${cfgList[@]}" =~ ${cfgName}2 ]] && [[ $singleComponentBuild || ${loadBuiltModules[@]} =~ "${gitDir}" ]];then
	openCfg[0,0]=$(<${tempDir}/module_cfg/${cfgName}2) ; openCfg[0,1]="${cfgName}2" ; echo ' config 2 found'
	elif [[ "${cfgList[@]}" =~ "${cfgName}.me" && -z $forceAutotools && ! -e ${tempDir}/disableMesonBuild.tmp ]];then
	openCfg[0,0]=$(<${tempDir}/module_cfg/${cfgName}.me) ; openCfg[0,1]="${cfgName}.me" ; echo ' meson config found'
	elif [[ "${cfgList[@]}" =~ "${cfgName}" ]];then
	openCfg[0,0]=$(<${tempDir}/module_cfg/${cfgName}) ; openCfg[0,1]="${cfgName}" ; echo ' config found'
	fi

	if [[ ${openCfg[0,0]} ]];then
		if [[ -z $overwriteCfgOn ]];then
		echo -e " Appending custom configuration file \e[1;34m${openCfg[0,1]}\e[1;37m to global config."
		configureFlags=$(eval echo ${configureFlagsDefault} ${openCfg[0,0]} ${manualFlag}) ; openCfg=()
		else
		echo -e " Using custom configuration file \e[1;34m${openCfg[0,1]}\e[1;37m, overriding global config."
		configureFlags=$(eval echo ${openCfg[0,0]} ${manualFlag}) ; openCfg=()
		fi
	else configureFlags=$(echo ${configureFlagsDefault} ${manualFlag})
	fi
else configureFlags=$(echo ${configureFlagsDefault} ${manualFlag})
fi

restoreConfigureFlags="$configureFlags"
restoreBuildDir="$buildDir"
restoreSysLocalOff="$sysLocalOff"
restoreAutogensource="$autogensource"

[[ -e ${userDirectory}/module_prep/${preperationBeforeConfigure} ]]&& { echo -e " Using preparation file ${userDirectory}/module_prep/\e[1;34m${preperationBeforeConfigure}\e[1;37m."
restore='yes'
source ${userDirectory}/module_prep/${preperationBeforeConfigure} ; }

[[ $sysLocalOff ]]&& { configureFlags="${configureFlags/--sysconfdir*\/etc*localstatedir*\/var/}" ; }

echo -e "\e[1;37m${line}"
echo -en "    \e[0;36mConfiguring ${gitDir}.    \e[1;37m${current}\e[0;37m/\e[1;31m${t}  \e[36;32m${percent}"
[[ $(<${userDirectory}/module_progress/skipped.modules) ]]&& { echo -en "\e[1;33m + ${sk} skipped\e[0;37m = ${originalCurrent}/${ot}" ; }
echo -e "\n\e[1;37m${line}\e[0;37m"

[[ -e ${userDirectory}/module_env/${envName} && -z $keepEnvOn ]]&& { echo -en "\e[1;37m"
if [[ -z $appendEnvOn ]];then echo -e " Using custom environment file ${userDirectory}/module_env/\e[1;34m${envName}\e[0;37m."
source ${userDirectory}/module_env/${envName}
else echo -e " Appending custom environment file ${userDirectory}/module_env/\e[1;34m${envName}\e[0;37m."
eval '$(sed -e 's_\(.*\)=\"_\1=\"\$\{\1\} _' ${userDirectory}/module_env/${envName})'
fi ; }

if [[ -e ${userDirectory}/module_custom_build/${originalName}.cb ]];then echo " Custom ${originalName}.cb found" ; source ${userDirectory}/module_custom_build/${originalName}.cb

else if [[ -z $noCleanBuildDir ]];then [[ $buildDir ]]&& { mkdir -p $buildSubDirectory ; cd $buildSubDirectory ; }

		if [[ -e ${userDirectory}/module_configure/${originalName}.configure ]];then echo " Custom ${originalName}.configure found"
		source ${userDirectory}/module_configure/${originalName}.configure

		elif [[ -e ${buildDir}./meson.build && -z $forceAutotools && -z $standard && ! -e ${tempDir}/disableMesonBuild.tmp ]];then upDirectory='..'
			[[ $ARCH = 'i686' ]]&& { restore='yes ' ; configureFlags="${configureFlags/$hostOption}" ; crossFile="--cross-file $crossFilePath" ; }
			[[ ! $configureFlags =~ '--buildtype=' ]]&& { restore='yes ' ; configureFlags+=' --buildtype=plain' ; configCommand="meson setup ${crossFile}" ; }

		elif [[ -e ${buildDir}./waf ]];then restore='yes' ; buildDir=() ; cd .. ; configureFlags="--out=$buildSubDirectory ${configureFlags/--sysconfdir*\/etc*localstatedir*\/var/}" ; configCommand="./waf configure"

		elif [[ -e ${buildDir}./bootstrap.sh && -z $standard ]];then ${buildDir}./bootstrap.sh && [[ -e ${buildDir}./configure ]]&& configCommand="${buildDir}./configure"
		elif [[ -e ${buildDir}./bootstrap && -z $standard ]];then ${buildDir}./bootstrap && [[ -e ${buildDir}./configure ]]&& configCommand="${buildDir}./configure"

		elif [[ $autogensub ]];then ${buildDir}./autogen.sh ; [[ -e ${buildDir}./configure ]]&& configCommand="${buildDir}./configure"
		elif [[ $autogensource ]];then (cd .. ; ./autogen.sh) ; [[ -e ${buildDir}./configure ]]&& configCommand="${buildDir}./configure"
		elif [[ -e ${buildDir}./autogen.sh && -z $standard ]];then configCommand=("${buildDir}./autogen.sh" "${buildDir}./configure")

		elif [[ -e ${buildDir}./configure ]];then [[ -e ${buildDir}./buildconf && -z $standard ]]&& configCommand=("${buildDir}./buildconf" "${buildDir}./configure") || configCommand="${buildDir}./configure"

		elif [[ -e ${buildDir}./CMakeLists.txt ]];then upDirectory='..' ; configCommand="cmake"
		configureFlags="-S ${tempDir}/${originalName} -B ${tempDir}/${originalName}/${buildSubDirectory} $cmakeFlags --install-prefix=$xbuild"
		for i in $(find ${buildDir}./ -name CMakeLists.txt); do
		sed -i "s@DESTINATION lib@DESTINATION lib${libdirsuffix}@" "$i"
		done
		fi

		for runConfig in "${configCommand[@]}" ; do $envVariable $runConfig $configureFlags $upDirectory 2> config_error.log || exitOnError ; done
	fi
echo -e "\e[1;30m"

	if [[ ! -e ${userDirectory}/module_custom_build/${originalName}.cb ]];then
		compileTime="$SECONDS"
		if [[ -e build.ninja ]];then ninjaBuild='yes' ; ninja -j$threads 2> build_error.log || exitOnError
		elif [[ -e waf ]];then ./waf || exitOnError
		else make DCMAKE_INSTALL_PREFIX=${xbuild} -j$threads $doNotHaltOnErrors $verbose 2> /tmp/build_error.log || :
		fi
		compileTime=$(($SECONDS - $compileTime))
	fi
fi

if [[ $? = 0 ]];then [[ $buildOnly ]]&& exit 1
echo -e "\e[0;37m"

	if [[ $ninjaBuild ]];then DESTDIR=${tempDir}/${originalName}_Files${version[0,0]} ninja install 2> install_error.log || exitOnError
	elif [[ -e Makefile || -e makefile || -e GNUmakefile ]];then
	make DCMAKE_INSTALL_PREFIX=${xbuild} -j${threads} $doNotHaltOnErrors install DESTDIR=${tempDir}/${originalName}_Files${version[0,0]} 2> install_error.log || :
	elif [[ -e waf ]];then DESTDIR=${tempDir}/${originalName}_Files${version[0,0]} ./waf install || exitOnError
	fi

cd ${tempDir}/${originalName}_Files${version[0,0]}

	if [[ -e ${buildDir}./CMakeLists.txt && $xbuild != '/usr' && -e ${tempDir}/${originalName}_Files${version[0,0]}/usr ]];then
	echo " Moving /usr to ${xbuild} in ${tempDir}/${originalName}_Files${version[0,0]}"
	mv ${tempDir}/${originalName}_Files${version[0,0]}/usr ${tempDir}/${originalName}_Files${version[0,0]}${xbuild}

	find . -name *.pc -type f -execdir sed -i "s@^prefix=.*@prefix=${xbuild}@" '{}' +
	fi
	
	if [[ -z $localDir && -e ${tempDir}/${originalName}_Files${version[0,0]}${xbuild}/local ]];then
	echo " Moving ${xbuild}/local to ${xbuild} in ${tempDir}/${originalName}_Files${version[0,0]}"
	mv ${tempDir}/${originalName}_Files${version[0,0]}${xbuild}/local/* ${tempDir}/${originalName}_Files${version[0,0]}${xbuild}
	[[ -s ${tempDir}/${originalName}_Files${version[0,0]}${xbuild}/local/ ]]&& echo " Local files remain." || rm -r ${tempDir}/${originalName}_Files${version[0,0]}${xbuild}/local/

	find . -name *.pc -type f -execdir sed -i "s@^libdir=.*@libdir=${prefix}/lib${libdirsuffix}@" '{}' +
	fi
	
	if [[ -e ${tempDir}/${originalName}_Files${version[0,0]}/${xbuild}/lib && "$libdirsuffix" ]];then
	echo " Moving $xbuild/lib to ${xbuild}/lib${libdirsuffix} in ${tempDir}/${originalName}_Files${version[0,0]}"
	mv ${tempDir}/${originalName}_Files${version[0,0]}/$xbuild/lib ${tempDir}/${originalName}_Files${version[0,0]}${xbuild}/lib${libdirsuffix}

	find . -name *.pc -type f -execdir sed -i "s@^libdir=.*@libdir=${prefix}/lib${libdirsuffix}@" '{}' +
	fi

sudo find . -name *.la -delete
compressManual
filePermissionsReset
[[ $stripDebugSymbols ]]&& stripSymbols

	[[ -e ${userDirectory}/module_preinstall/${preInstall} ]]&& { echo -e " Using pre-install file ${userDirectory}/module_preinstall/\e[1;34m${preInstall}\e[1;37m."
	source ${userDirectory}/module_preinstall/${preInstall}
	cd ${tempDir}/${originalName}_Files${version[0,0]} ; }

	if [[ $compressedArchive ]];then newInstall="${gitDir}-${ARCH}-$(date +%Y%m%d)"
	elif [[ $revert ]];then newInstall="${originalName}-$(git -C ${tempDir}/${gitDir} show-ref --head --hash head)-${ARCH}-$(date +%Y%m%d)"
	elif [[ $version ]];then newInstall="${originalName}-${version[0,0]}${version}${version[0,1]}-${ARCH}-$(date +%Y%m%d)"
	elif [[ -e ${sourceDirectory}/${gitDir}/.git/refs/heads/$(git rev-parse --abbrev-ref @) ]];then
	newInstall=$(git -C ${sourceDirectory}/${gitDir} rev-parse --abbrev-ref @)
	newInstall=$(<${sourceDirectory}/${gitDir}/.git/refs/heads/${newInstall})
	newInstall="${originalName}-${newInstall}-${ARCH}-$(date +%Y%m%d)"
	elif [[ -e ${parentDirectory}/${gitDir}/.git/refs/heads/$(git rev-parse --abbrev-ref @) ]];then
	newInstall=$(git -C ${parentDirectory}/${gitDir} rev-parse --abbrev-ref @)
	newInstall=$(<${parentDirectory}/${gitDir}/.git/refs/heads/${newInstall})
	newInstall="${originalName}-${newInstall}-${ARCH}-$(date +%Y%m%d)"
	elif [[ -e ${parentDirectory}/.git/refs/heads/$(git rev-parse --abbrev-ref @) ]];then
	newInstall=$(git -C ${parentDirectory}/.git/refs/heads rev-parse --abbrev-ref @)
	newInstall=$(<${parentDirectory}/.git/refs/heads/${newInstall})
	newInstall="${originalName}-${newInstall}-${ARCH}-$(date +%Y%m%d)"
	elif [[ -e ${sourceDirectory}/${gitDir}/.git/ORIG_HEAD ]];then
	newInstall="${originalName}-$(<${sourceDirectory}/${gitDir}/.git/ORIG_HEAD)-${ARCH}-$(date +%Y%m%d)"
	elif compgen -G ${tempDir}/${originalName}_Files${version[0,0]}/${xbuild}/lib${libdirsuffix}/pkgconfig/*.pc -quit > /dev/null ;then
	newInstall="${originalName}-$(echo $(grep -E -o "[[:space:]][[:digit:]].*" ${tempDir}/${originalName}_Files${version[0,0]}/${xbuild}/lib${libdirsuffix}/pkgconfig/*.pc))-${ARCH}-$(date +%Y%m%d)"
	elif compgen -G ${tempDir}/${originalName}_Files${version[0,0]}/${xbuild}/share/pkgconfig/*.pc -quit > /dev/null ;then
	newInstall="${originalName}-$(echo $(grep -E -o "[[:space:]][[:digit:]].*" ${tempDir}/${originalName}_Files${version[0,0]}/${xbuild}/share/pkgconfig/*.pc))-${ARCH}-$(date +%Y%m%d)"
	elif compgen -G ${tempDir}/${originalName}_Files${version[0,0]}/${xbuild}/local/lib${libdirsuffix}/pkgconfig/*.pc -quit > /dev/null ;then
	newInstall="${originalName}-$(echo $(grep -E -o "[[:space:]][[:digit:]].*" ${tempDir}/${originalName}_Files${version[0,0]}/${xbuild}/local/lib${libdirsuffix}/pkgconfig/*.pc))-${ARCH}-$(date +%Y%m%d)"
	else newInstall="${originalName}-noversion-${ARCH}-$(date +%Y%m%d)"
	fi

[[ $destdirOnly ]]&& exit 1

[[ -e ${tempDir}/${originalName}_Files${version[0,0]} ]]&& installComponent || { echo " ${tempDir}/${originalName}_Files${version[0,0]} does not exist. Aborting install." ; exit ; }

cd - &>/dev/null ; sudo rm -rf ${tempDir}/${originalName}_Files
fi

    if [[ $? = 0 ]];then echo -e " \e[1;34m${customList}${gitDir}\e[1;37m built."

    [[ -e ${userDirectory}/module_postinstall/${postInstall} ]]&& { echo -e " Using post install file ${userDirectory}/module_postinstall/\e[1;34m${postInstall}\e[1;37m."
    source ${userDirectory}/module_postinstall/${postInstall} ; }

cd $tempDir

    if [[ $singleComponentBuild ]];then echo " Single component ${gitDir} build not logged in *.modules lists."
    else loadBuiltModules+=("${componentDirectory[0]}"); echo "${componentDirectory[0]}" >> built.modules ; ((progress[1]++))
    componentDirectory=("${componentDirectory[@]:1}") ; printf '%s\n' "${componentDirectory[@]}" > build.modules ; ((progress[0]--))
    echo -e " \e[0;32mSuccess:\e[1;37m \e[0;33m$gitDir in ${tempDir}/build.modules\e[1;37m removed from resume list." ;
    fi

  echo " Deleting ${tempDir}/${gitDir}." ; sudo rm -rf ${tempDir}/${gitDir}/
  [[ -e ${tempDir}/disableMesonBuild.tmp ]]&& unlink ${tempDir}/disableMesonBuild.tmp

  [[ $restore ]] && {
  [[ $restoreBuildDir != "$buildDir" ]]&& buildDir="$restoreBuildDir"
  [[ $restoreSysLocalOff != "$sysLocalOff" ]]&& sysLocalOff="$restoreSysLocalOff"
  [[ $restoreAutogensource != "$autogensource" ]]&& autogensource="$restoreAutogensource"
  }
  [[ $restoreConfigureFlags != "$configureFlags" ]]&& configureFlags="$restoreConfigureFlags"

  ${compileTime:+echo -e " > Compile time: \e[1;34m${compileTime}\e[0ms"}
  
  unset version revert ninjaBuild upDirectory configCommand newInstall restoreConfigureFlags restore compileTime

  else exitOnError
  fi
done

saveProgress ; echo -e " $SECONDS seconds elapsed."
)
