# xbuild
[xbuild](https://gitlab.freedesktop.org/2A4U/xbuild) can build sources from git or archives including X. xbuild can install programs to '/usr' or use a $HOME directory to create a build and install environment separate from the system directories of a distro.

## Quick Start
1. `git clone --depth 1 https://gitlab.freedesktop.org/2A4U/xbuild $HOME/xbuild`
2. `~/xbuild/setup.bash --quick` — clone and build X.

## Todo
- [ ] Test `setup.bash --quick`
- [ ] Vulkan SDK.
- [ ] Test `setup.bash --wayland`

## Prerequisite
- free space in $tempDir (default /dev/shm) — used to compile sources.

## Getting Started
1. Create a new user, e.g.: `sudo useradd new_$USER`.
2. Give new_$USER sudo capability `sudo usermod -a -G sudo new_$USER`.
3. Check if "%sudo ALL=(ALL) NOPASSWD: ALL" is uncommented: `grep '%sudo' /etc/sudoers`. Uncomment '%sudo' line if needed: `sudo mc -e /etc/sudoers`.
4. Optionally, add other group permissions to new_$USER, e.g.: `sudo usermod -a -G audio new_$USER`.
5. Login as new_$USER.
6. `git clone --depth 1 https://gitlab.freedesktop.org/2A4U/xbuild`
7. `~/xbuild/setup.bash --update` — check for `sudo`, copy-xbuild-files-to-$userDirectory wizard. `--update` will overwrite default files in $HOME/xuser.
8. Optionally, follow the steps at (llvm-build)[https://gitlab.freedesktop.org/2A4U/llvm-build] to `build llvm-project` before building X.
9. Optionally, edit $userDirectory/*build.environment*. The $userDirectory default is '$HOME/xuser'.
10. `build` — if no directory or other options follow the `build` command, then `build` resumes from build queue.

## *setup.bash* — Options

Syntax: `./setup.bash --option=''`

Overrides|Default
-|-
`userdir`|user settings location ($HOME/xuser)
`xbuild`|library install prefix ($HOME/x)
`xsrc`|sources location (/usr/src/xsrc)
`tempdir`|build location (/dev/shm/x)
`installlogdir`|install logs location ($HOME/xinstalled[64, ]$installSuffix)
`removelogdir`|remove logs location ($HOME/xremoved[64, ]$installSuffix)
`archivelogdir`|archive logs location ($HOME/xarchive[64, ]$installSuffix)
`CFLAGS`|-O3 -march=native -pipe -fPIC
`CXXFLAGS`|${CFLAGS} -fvisibility-inlines-hidden}
`CCACHE_DIR`|compile cache directory
`crossfile`|cross file path (${userDirectory}/meson_i686_cross_compilation.txt})
`clone`|clone from a git list — *base.modules* is the default list unless a custom list (\*.***modules***) is used as a value<br/>e.g. `./setup.bash --clone='my_git_list.modules'`<br/>*my_git_list.modules* requires a correspoding *my_git_list.**url*** file.
`submodule`¹|install or update git submodules
`unshallow`¹|install full git history
`wayland`¹|configure for building wayland
`xorg`¹|configure for building without wayland
`update`¹|force update of user settings if xbuild git settings are newer

¹ Stand alone argument — does not use a value.

To revert to a default setup value on an option use an equal sign with an empty value, e.g. `./setup xbuild=''`.

## Other $USERs can execute compiled sources
Modify and add the following to '**other**' $USER's '*.bash_profile*' ($HOME/x64 contain the files to be executed):
```
export PATH="$HOME/x64/bin:$PATH"
export LD_LIBRARY_PATH="HOME/x64/lib64"
```
Editing /etc/*ld.so.conf* may be needed so **other** users can find files in non-standard paths. As the '**xbuild user**', `sudo echo "$HOME/x/lib" >> /etc/ld.so.conf ; sudo ldconfig` and `sudo echo "$HOME/$xbuild/lib64" >> /etc/ld.so.conf ; sudo ldconfig`.

## `build` — Command Arguments

Syntax: `build option`

Arguments|Description
-|-
`--*`|Append to configure options.<br/>e.g. `build --prefix=/usr`
`*=*`|Append to environment variables.<br/>e.g. `build CFLAGS='-Os'`
`*.diff` `*.patch` `*.tar.?z*` `*.[gx]z` `*.tar.zst` `*.zst`|Patch file name.<br/>e.g. `build mesa abc.diff.tar.zst`<br/>Or, extract and compile archive.<br/>e.g. `build mesa-main.tar.bz2`
`patchoff` `[pP]` `po`|Disable applying patches listed in component *\*.mend* file.
`createpatch` `patch`|Create a patch. If there is a second argument after `createpatch`, then the second argument will be the patch name.
`reverse`|Reverse patch.
`restart` `refresh` `renew` `re` `[rR]`|*build.modules* (resume list or build queue) is recreated for a new build.
`list` `repolist`|Create a list of urls from current build queue (*build.modules*) to $tempDir/*url.modules*
`buildonly` `noinstall`|Build only. Do not install.
`destdironly`|Install to $DESTDIR only. Removes local files for a component found in previous build log.
`test` `dryrun`|Install to $DESTDIR only. Keep local files.
`skip` `[sS]`|Skip modules that produce an error and continue building the next module in queue.
`keepenv` `preserveDefaultEnv` `[eE]`|Preserve environment variables (overrides option `appendenv` — append custom environment variables).
`appendenv` `ae`|Append custom environment variables instead of overwriting default environment.
`keepCfg` `[gG]`|Ignore custom *\*.cfg*.  Preserve global config instead of appending/overwriting with custom *\*.cfg*.
`overwriteCfg` `[oO]`|Overwrite global config instead of appending (default behaviour) with custom *\*.cfg.*.
`syslocaloff` `sysoff` `so`| Disable sysconfdir and localstatedir variables from configureFlags.
`*.cfg?*`|Config file to append to global config.<br/>e.g. `build mesa $HOME/xuser/module_cfg/mesa.cfg2.me`
`editmake` `editMake` `em`|Remove subdirectories listed in *Makefile.am* files: nls; specs; doc[s]; man; examples; demos; test.
`withnotests` `testsoff` `ts`|Disable build of "tests" subdirectories in Makefile.am files. Used with `editmake`.<br/>e.g. `build editmake withnotests`
`resume` `cleanOff` `noclean` `rb`|Do not delete build directory from $tempDir before resuming component build.
`force` `fo`|Ignore build errors. Continue build.
`standard` `standardcfg`|Force standard configure. Disable: meson; bootstrap(.sh); autogen.sh; buildconf.
`local` `keeplocal`|Do not force install to $xbuild directory if local directory install exists.
`withautotools` `forceautotools`|Build with autotools instead of meson.
`autogensub`|Run autogen.sh without $configureFlags from build directory ($buildSubDirectory).
`autogensource`|Run autogen.sh without $configureFlags from source directory ($gitDir).
`mallocOff` `mallocoff` `mo`|Disable malloc check during configure.
`debug`|Compile binaries with debugging.
`strip` `resize` `shrink`|Remove debug symbols.
`verbose` `v`|Show extra compiling messages.
`stable` `tag` `tagged`|Build tag or git hash referenced to in *component.version* file.<br/>e.g. `build stable mesa`
`version` `git` `checkout` `revision` `hash` `branch`|The following argument after `version` will build from a git tag, hash or branch.<br/>e.g. `build mesa version abc123`
`revert`|Revert the number of commits by following argument.<br/>e.g. `build mesa revert 0`
`unshallow`|Unshallow or retrieve git history in $sourceDirectory<br/>e.g. `build unshallow update mesa`<br/>Or unshallow only $tempDir/gitDir with `revert` option.<br/>e.g. `build unshallow mesa revert 1`
`copygit` `withgit`|Copy .git to $tempDir.
`backup`|Backup files to $tempDir before removing.
`movebackup` `savebackup` `save`|Write backup files to $HOME/xbackup${libdirsuffix}${installSuffix} before removing.
`keepinstall` `keepkg` `removepkgno`|Do not remove files listed in component install log before build.
`32-bit` `32bit` `32`|Build 32-bit version of component.<br/>e.g. `build 32-bit mesa`
`after` `a`|Build after component in queue.<br/>e.g. `build after mesa` or `build after libdrm to mesa`
`before` `b`|Build before component in queue.<br/>e.g. `build before mesa` or `build after libdrm before mesa`
`from` `f`|Build from component in queue.<br/>e.g. `build from mesa` or `build from libddrm to mesa`
`to` `t`|Build to component in queue.<br/>e.g. `build to mesa` or `build from mesa to mesa`

## `build update` — git Command Arguments
Arguments|Description
-|-
`update` `updated` `[uU]` `up`|Update from git remote.<br/>e.g. `build update`
`changes`|Build only changes since update.
`repo` `repository`|Update repository after module url "list" creation or git remote "url" change.<br/>e.g. `build updated repository list`
`now`|Ignore update lock timer — default 3600 seconds or 1 hour.<br/>e.g. `build update now`
`only`|Update only. Do not build.<br/>e.g. `build update only`
`branch` `ub`|Auto-update default branch if, for example, it changed from 'master' to 'main'.<br/>e.g. `build update branch only`
`merge`|Merge remote and local files.<br/>e.g. `build update merge`
`gitreset` `resethard`|Match remote repository and delete local changes.<br/>e.g. `build updated gitreset`
`main` `checkoutmain`|Check out main branch.<br/>e.g. `build updated main`
`master` `checkoutmaster`|Check out master branch.<br/>e.g. `build updated master`


## Tips and Aliases
- `bu` — an alias command equivalent to `build updated changes`. Use `bu` to build only when changes from remote repository are detected.
- `cdx` — change to $sourceDirectory.

## Examples and Typical Use
- `cdx ; re ; build update` — change to $sourceDirectory, reset build queue, update and build all of X.
- `bu vlc withgit` — update vlc multimedia source and copy '.git' to $tempDir.
- `bu mesa save backup` — backup current install and build mesa if remote git has changed.

## Building 32-bit X Libraries
- `build 32-bit 32-bit.modules`

## Notes and Behaviour
- Single component building appends the second pass configuration variables (*\*.cfg2* or *\*.cfg2.me*) if available, otherwise append first past variables or use default configuration variables.
- `build` only copies '.git' directories to $tempDir if arguments `version` `stable` `revert` `previous` `createpatch` are used.

### 5 Ways of Single Component Building e.g.:
- `build directory_name` method can build sources not part of X.
- `cd directory_name ; build` likewise as above.
- `cdx ; build mesa/mesa`
- `cd $sourceDirectory/mesa/mesa ; build`
- `cdx ; build mesa`
- `build component_directory_name` will only find X component sources in subdirectories if $PWD is $sourceDirectory. Use `cdx` to change $PWD to $sourceDirectory before `build X_component`.

### Updating
- `update` updates current checkout in the $sourceDirectory which may not be the default branch.
- `now` is not needed to disable the `update` lock timer when building a single component (not building from a list).

### Resuming and Restarting Build Queue
- `build` resumes from build queue ($HOME/module_progress/*build.modules*) by default. Reset a build queue to the beginning with `restart` or `build restart`.
- `restart` is not required when building from a different custom list. For example, `build my_custom.list` will regenerate the build queue if the previous build queue was `build another_custom.list`.
- `build` will search for custom lists in $HOME/module_list or $PWD (current directory).

### Cleaning and $tempDir
- `build` deletes previous temporary build files in $tempDir prior to starting a new build.
- `revert` only acts on copies of $sourceDirectory components located in $tempDir. Reverted commits are not saved on subsequent builds unless `build noclean` is used. Therefore, `build mesa revert 1 ; build mesa revert 1` will build the same commit. `build mesa revert 1; build mesa revert 1 noclean` will build different commits. Incremental reverting without `noclean` is useful for finding bugs.

## Double Pass Build How-to
1. In $HOME/module_cfg create a config file with extension *\*.cfg* (autotools config) or *\*.cfg.me* (meson config) for a component directory — console command:
```
touch $HOME/module_cfg/component_directory_name.cfg.me
```
2. Create a second config file with extension *\*.cfg**2*** or *\*.cfg**2**.me*.
```
touch $HOME/module_cfg/component_directory_name.cfg2.me
```
3. Create a custom list in $HOME/module_list/*my_custom.list* with 'component_directory_name' listed twice — once per line, then:
```
build my_custom.list
```
- Alternatively:
```
build my_custom.list from component_directory_name to component_directory_name
```

## Creating a Patch
1. `build component_directory_name createpatch` creates a copy of the source directory to $tempDir.
2. Edit source directory files in $tempDir.
3. Rerun `build component_directory_name createpatch patch_name` to create a patch in $HOME/module_patch/*patch_name.patch* and $tempDir/*patch_name.patch*. If 'patch_name' is omitted, then 'patch_name' will default as 'component_directory_name'.

Note: *patch_name.patch* is added to $HOME/module_mend/*component_directory_name.mend* and applied to $tempDir/$gitDir on next `build`. `rm $HOME/module_mend/component_directory_name.mend` to remove this behaviour or `build patchoff` to skip auto-patching.

## Creating a Plugin Script
Plugin scripts bypass most of the [build mechanisms](https://gitlab.freedesktop.org/2A4U/xbuild#build-mechanism) of `build`.

A typical xbuild plugin script may include these functions:
- `update`
- `rmodule` - deletes files and log of previous install.
- `stripSymbols`
- `compressManual`
- `filePermissionsReset`
- `installComponent`

1. Make a plugin directory 1 level deep from the $HOME directory: `mkdir $HOME/custom_plugin`.
2. Copy and rename [plugin.template](https://gitlab.freedesktop.org/2A4U/xbuild/blob/master/plugin.template) to '$HOME/custom_plugin/*build.directory_name_of_source*': `cp $HOME/xbuild/plugin.template $HOME/custom_plugin/build.directory_name_of_source`.
3. Edit `mc -e $HOME/custom_plugin/build.directory_name_of_source`.
4. `linkplugin` - searches for *build.\** files 1 level deep from home directory that match any directory in $sourceDirectory. `linkplugin` then creates a link in $HOME/module_plugin to $HOME/custom_plugin/*build.directory_name_of_source*.
5. `build directory_name_of_source` - executes custom plugin script.

## Compiling only Mesa git

1. `mkdir -p /usr/src/xsrc ; cd /usr/src/xsrc ; git clone --depth 1 https://gitlab.freedesktop.org/mesa/mesa`
2. `git clone --depth 1 https://gitlab.freedesktop.org/2A4U/xbuild $HOME/xbuild`
3. `~/xbuild/setup.bash --xbuild=/usr`
4. [Boot to console](https://duckduckgo.com/?q=linux+login+console+text+mode) — login through text mode.
5. Uninstall distro mesa package.
6. `build mesa`
7. `startx`

## Removing a Module Manually
Syntax (from $sourceDirectory): `rmodule module_name options`

Usage from another directory e.g. `cd ~/xinstalled ; rmodule mesa`

Arguments|Description
-|-
`32-bit`|Removes 32-bit libraries.<br/>e.g. `rmodule mesa 32-bit`
`backup`|Backup files to $tempDir before removing.<br/>e.g. `rmodule mesa backup`
`movebackup` `savebackup` `save`|Write backup files to $HOME/xbackup${libdirsuffix}${installSuffix} before removing.<br/>e.g. `rmodule mesa save backup`

## Recovering a Module Backup
Syntax: `recover module_name` — Wildcards can be used.

`recover` will end search for the first *\*.tar.zst* files found in $PWD (current directory), $tempDir or $HOME/xbackup${libdirsuffix}${installSuffix}.

## File Location
File Location|Short Description
-|-
$userDirectory|user copy of xbuild folder
$HOME/*.bash_profile*|link to $HOME/xbuild/*bash_profile.bash*
$HOME/xbuild/*bash_profile.bash*|main script
$HOME/xbuild/*setup.bash*|first run wizard
$HOME/xbuild/*alias*|alias commands
$HOME/*.alias*|link or copy of $HOME/xbuild/*alias*
$HOME/xbuild/*build.order*|full X build order
$HOME/xbuild/*plugin.template*|
$HOME/xbuild/*build.maintenance*|`build`, git and file operations functions
$userDirectory/*build.environment*|default variables
$userDirectory/*build.variables*|compile variables
$userDirectory/*update_exclusion.modules*|modules to be excluded from updating
$userDirectory/*exclude.list*|modules to exclude from default build list

## User Variables — *build.environment*
Variable|Default|Description
-|-|-
tempDir|/dev/shm/x|Compile path.
xbuild|$HOME/x|Install path. Use `./setup.bash xbuild='/usr'` to install to system.
CCACHE_DIR|$HOME/ccache|Used to speed up subsequent meson builds.
updateTimer|3600|Seconds before script allows subsequent update attempt on a component.
libdirsuffix|64 (in 32-bit mode the default is empty)|Suffix appended to '$LIBDIR'.<br/>e.g. libraries will install to '/usr/lib64'
sourceDirectory|$HOME/xsrc|Sources location. *setup.bash* gives an option to create a link to '/usr/src/xsrc'.
crossFilePath|$userDirectory/meson_i686_cross_compilation.txt|
CFLAGS|-O2 -march=native -pipe -fPIC|
CXXFLAGS|$CFLAGS -fvisibility-inlines-hidden|
hostOption|--host=i686-pc-linux-gnu --build=x86_64-pc-linux-gnu --target=x86_64-pc-linux-gnu|

## Build Mechanism
*build.component* → update → delete $tempDir/$gitDir → extract archive → set version → copy source to $tempDir → create patch → backup → delete previous module install → move install log → patch $tempDir/$gitDir  → *component.prep* → edit *Makefile.am* → edit Malloc in *configure.ac* → detect config file → merge default and user configure flags → set environment flags → *component.custombuild* or *component.configure* → detect configuration → configure → compile → install to $DESTDIR → compress manual → change file permissions → strip symbols → delete *\*.la* → *component.pre* → create *component.installed* → install → delete $DESTDIR → *component.post* → delete $tempDir/$gitDir

## Acknowledgement
- [Patrick J. Volkerding](www.slackware.com) - manual compression, file permissions, and strip symbols code.
