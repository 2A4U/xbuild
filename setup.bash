#!/usr/bin/env bash
[[ "${BASH_SOURCE:0:1}" = '.' ]]&& setupDirectory="$PWD" || setupDirectory="${BASH_SOURCE%/*}"

[[ ! -e ~/.bash_profile ]]&&{ echo '#!/usr/bin/env bash' >> ~/.bash_profile ; echo -e " ~/\e[0;36m.bash_profile\e[0m created." ; }

varOptions () {
if [[ $3 == *'.me' ]];then preform='-D' ; config=$3
elif [[ -e $3 ]];then preform='' ; config=$3
else preform='export ' ; config=~/.bash_profile
fi

if [[ -z $2 ]];then sed -i "0,/^${preform}${1}/d" $config &&
${3:-echo -e " \e[1;30m${1} \e[0;33m\xe2\x96\xb6\e[0m default (\e[0;3mbuild.environment -- \
$(grep --max-count 1 --only-matching "${1}.*" ${userDirectory}/build.environment)\e[0m)"}
elif ! grep --quiet -m 1 "^${preform}${1}=" $config ;then
echo "${preform}${1}=\"${2}\"" >> $config && echo -e " \e[1;30m${1} \xe2\x96\xb6 \e[0;36m${2}\e[0m ${3:+\xe2\x96\xb6 $3}"
else sed -i "0,/re/s@\(${1}=\).*@\1\"${2}\"@" $config && echo -e " \e[1;30m${1} \xe2\x96\xb6 \e[0;36m${2}\e[0m ${3:+\xe2\x96\xb6 $3}"
fi
}

options=$(getopt -o 'h' --longoptions 'clone::,submodule,unshallow,quick,update,extra,wayland,xorg,\
userdir:,xsrc:,xbuild:,tempdir:,installlogdir:,removelogdir:,archivelogdir:,CFLAGS:,CXXFLAGS:,CCACHE_DIR:,crossfile:' -- "$@")

[[ $? -eq 0 ]]||{ echo -e " \e[5m\xE2\x9A\xA0 \e[0mIncorrect option(s)." ; exit ; }
eval set -- "$options"
unset options

while : ; do case "$1" in
	'--userdir') varOptions userDirectory $2
	shift 2 ; continue ;;

	'--xbuild') varOptions xbuild $2
	shift 2 ; continue ;;

	'--xsrc') varOptions sourceDirectory $2
	shift 2 ; continue ;;

	'--tempdir') varOptions tempDir $2
	shift 2 ; continue ;;

	'--installlogdir') varOptions logInstallDirectory $2
	shift 2 ; continue ;;

	'--removelogdir') varOptions logRemoveDirectory $2
	shift 2 ; continue ;;

	'--archivelogdir') varOptions logArchiveDirectory $2
	shift 2 ; continue ;;

	'--CFLAGS') varOptions CFLAGS $2
	shift 2 ; continue ;;

	'--CXXFLAGS') varOptions CXXFLAGS $2
	shift 2 ; continue ;;

	'--CCACHE_DIR') varOptions CCACHE_DIR $2
	shift 2 ; continue ;;

	'--crossfile') varOptions crossFilePath $2
	shift 2 ; continue ;;

	'--clone') clone='yes' ; base=$2
	shift 2 ; continue ;;

	'--submodule') declare {clone,installUpdateSubmodule}='yes'
	shift ; continue ;;

	'--unshallow') declare {clone,unshallow}='yes'
	shift ; continue ;;

	'--quick') declare {clone,async}='yes'
	shift ; continue ;;

	'--wayland') wayland='yes'
	shift ; continue ;;

	'--xorg') xorg='yes'
	shift ; continue ;;

	'--update') update='yes' ; echo " Updating user directory (${userDirectory}) files if source is newer."
	shift ; continue ;;

	'--') shift ; break ;;
	*) echo ' Internal error.' >&2 && exit 1 ;;
	esac
done

[[ ! $PATH == *'/sbin:'* ]]&& export PATH="/sbin:$PATH"

if ! grep -qm 1 userDirectory ~/.bash_profile ;then varOptions userDirectory ~/xuser ;fi

varOptions setupSource $setupDirectory

[[ -d /run/systemd/system ]]&& varOptions systemd yes

if lspci | grep -qm 1 VGA.*AMD ;then varOptions amdgpu yes ;fi

readarray -t bashProfile <~/.bash_profile
[[ ${bashProfile[-1]} = "source ${setupDirectory}/bash_profile.bash" ]]||{
	for lastLine in "${!bashProfile[@]}" ; do
	[[ "${bashProfile[lastLine]}" == *source*bash_profile.bash* ]]&&{
	unset bashProfile[lastLine] ; set "${bashProfile[@]}" ; }
	done
printf '%s\n' "${bashProfile[@]}" "source ${setupDirectory}/bash_profile.bash" > ~/.bash_profile
}

source ~/.bash_profile

cd ${setupDirectory}

[[ $update = 'yes' ]]&& unset update|| update='--ignore-existing'

rsyncf () { rsync --recursive --update --archive --whole-file --inplace --no-compress ${update} ${@} ; }

rsyncf build.environment build.variables build.order exclude.list meson_i686_cross_compilation.txt ${userDirectory}/

mkdir -p ${userDirectory}/module_{cfg,configure,custom_build,env,list,mend,patch,plugin,postinstall,preinstall,progress,prep,version}

mkdir -p ~/x{installed,removed,archive,backup}{,${libdirsuffix}}${installSuffix}

rsyncf ${setupDirectory}/module_{cfg,configure,custom_build,list,patch,preinstall,prep} ${userDirectory}/

[[ ! -e ${userDirectory}/module_progress/build.modules ]]&& resetHistoryBuildExclude

[[ ! -e ${userDirectory}/update_exclusion.modules ]]&& touch ${userDirectory}/update_exclusion.modules

linkplugin

sed -i "0,/re/s@\(llvm-config\).*@\1 = \'${xbuild//[[:digit:]]}/bin/llvm-config\'@" ${userDirectory}/meson_i686_cross_compilation.txt &&
echo -e " \e[1;30mllvm-config \xe2\x96\xb6 \e[0;36m${xbuild//[[:digit:]]}/bin/llvm-config\e[0m \xe2\x96\xb6 ${userDirectory}/meson_i686_cross_compilation.txt"

[[ $amdgpu ]]&&{
for cfgPass in '' '2' ; do 
varOptions gallium-drivers "radeonsi,svga,swrast" ${userDirectory}/module_cfg/mesa.cfg${cfgPass}.me
varOptions vulkan-drivers amd ${userDirectory}/module_cfg/mesa.cfg${cfgPass}.me
done
}

xcfg=${userDirectory}/module_cfg/xorg-server.cfg.me

[[ $wayland ]]&&{
varOptions xorg false $xcfg
varOptions xwayland true $xcfg
varOptions xwayland_eglstream true $xcfg
varOptions platforms x11,wayland ${userDirectory}/module_cfg/mesa.cfg.me
varOptions platforms x11,wayland ${userDirectory}/module_cfg/mesa.cfg2.me
varOptions enable-wayland true ${userDirectory}/module_cfg/libxkbcommon.cfg.me
}

[[ $xorg ]]&&{
varOptions xorg true $xcfg
varOptions xwayland false $xcfg
varOptions xwayland_eglstream false $xcfg
varOptions platforms x11 ${userDirectory}/module_cfg/mesa.cfg.me
varOptions platforms x11 ${userDirectory}/module_cfg/mesa.cfg2.me
varOptions enable-wayland false ${userDirectory}/module_cfg/libxkbcommon.cfg.me
[[ $systemd ]]&&{ varOptions systemd_logind true $xcfg || varOptions systemd_logind false $xcfg ; }
}

sudoStatus=$(sudo -nv &>/dev/null)
if [[ $? = 0 ]];then echo ' Sudo requirement met.'
elif echo "$sudoStatus" | grep -q '^sudo:' ;then echo -e " \e[5m\xE2\x9A\xA0 \e[0mInstalls will be interupted by sudo password requests.\n\
 Add line '\e[0;36m$USER ALL=(ALL) NOPASSWD: ALL' in '/etc/sudoers\e[0m' to avoid password prompts.\n\
 Or set '\e[0;36m%sudo ALL=(ALL) NOPASSWD: ALL\e[0m' in '\e[0;36m/etc/sudoers\e[0m' and add user '\e[0;36m$USER\e[0m' to group sudo\n\
 with command '\e[0;36msudo usermod --append --groups sudo $USER\e[0m' to allow users in group sudo to run sudo commands."
exitOrContinue
else echo ' Sudo permission not available--required to set Xorg ownership to root.root and permission to u+s to start X.'
exitOrContinue
fi

writeAdvanceTmp100 () (
flock -n 100
readarray -t advance < ${tempdir}/advance.tmp
advance=( "${advance[@]:0:$i}" "${advance[i]/ cloned} cloned" "${advance[@]:((i+1))}" )
printf '%s\n' "${advance[@]}" > ${tempdir}/advance.tmp
)

writeAdvanceTmp200 () (
flock -n 200
readarray -t advance < ${tempdir}/advance.tmp
advance=( "${advance[@]:0:$ii}" "${advance[ii]/ built} built" "${advance[@]:((ii+1))}" )
printf '%s\n' "${advance[@]}" > ${tempdir}/advance.tmp
)

asyncBuild () { echo ' Asynchronous build start.'

for (( ii='0'; ii<arrayTotal; ii++ )) ; do readarray -t advance < ${tempdir}/advance.tmp

	for (( s='0'; s<30; s++ )) ; do
		if [[ ! -e ${tempdir}/advance.tmp && ii = 0 ]]||[[ ! ${advance[ii]} == *' clone' && ! "${advance[ii]}" == *' built' && ii = 0 ]];then
		sleep 2
		elif [[ $s = 29 ]];then echo -e " Clone of \e[0;36m${advance[ii]}\e[0m not available. Exiting build function." ; exit
		else break
		fi
	done

	while [[ ! "${advance[ii]}" == *' clone' ]]; do
	sleep 1
	readarray -t advance < ${tempdir}/advance.tmp
	done

	while [[ ! ${advance[ii]} == *' built' && $ii -ge 0 && $ii -le "$arrayTotal" ]]; do
	sleep 1
	readarray -t advance < ${tempdir}/advance.tmp
	[[ ${advance[ii]} == *' clone' ]]&& break
	done

	if [[ ${advance[ii]} == *' clone' ]];then
	build "${advance[ii]/[[:space:]]*}"
	[[ $? = 0 ]]&& { writeAdvanceTmp200 200>${tempdir}/advance-flock.tmp ; }||exit 1
	fi
done

readarray -t advance < ${tempdir}/advance.tmp

[[ $ii = "$arrayTotal" && "${advance[ii-1]}" == *' built' ]]&&{
rm ${tempdir}/advance{,-flock}.tmp ; }
}

clone () {
if [[ $installUpdateSubmodule ]];then gitCommand='git -C "${basePath[$i]}" submodule update --init --recursive --depth 1'
elif [[ $unshallow ]];then gitCommand='git -C "${basePath[$i]}" fetch --unshallow'
else gitCommand=('mkdir -p "${basePath[$i]}"' 'git clone --depth 1 --recursive "${concurrentURL[$i]}" "${basePath[$i]}"')
fi

for (( i='0'; i<arrayTotal; i++ )) ; do

	if [[ "${concurrentURL[$i]}" == *[[:space:]]* && -z "$installUpdateSubmodule" && -z "$unshallow" ]];then mkdir -p "${basePath[i]}"
	git clone --depth 1 --recursive --branch="${concurrentURL[$i]/*[[:space:]]}" "${concurrentURL[$i]/[[:space:]]*}" "${basePath[i]}"

	else for execute in "${gitCommand[@]}" ; do eval "$execute"; done
	fi

	[[ $async ]]&& writeAdvanceTmp100 100>${tempdir}/advance-flock.tmp

done
}

[[ $clone ]]&&{
base=${base:-base.modules}
readarray -t basePath < ${userDirectory}/module_list/${base}||exit 1
readarray -t concurrentURL < ${userDirectory}/module_list/${base/$*.modules/.url}
arrayTotal="${#basePath[@]}"

if grep -qm 1 "^export sourceDirectory" ~/.bash_profile && [[ ! $sourceDirectory = /usr/src/xsrc ]];then
ln --symbolic $sourceDirectory ~/xsrc
elif [[ -e ~/xsrc && $sourceDirectory = /usr/src/xsrc && ! -e /usr/src/xsrc ]];then ln --symbolic ~/xsrc /usr/src/xsrc
elif [[ $sourceDirectory = /usr/src/xsrc && ! -e ~/xsrc ]];then ln --symbolic $sourceDirectory ~
elif [[ $sourceDirectory = ~/xsrc && ! -e /usr/src/xsrc ]];then ln --symbolic $sourceDirectory /usr/src/xsrc
fi

mkdir -p $sourceDirectory
cd $sourceDirectory

[[ $async ]]&& { mkdir -p $tempdir ; printf '%s\n' "${basePath[@]}" > ${tempdir}/advance.tmp ; clone & sleep 5 ; asyncBuild ; }||clone
}

[[ $firstRunComplete ]]|| { varOptions firstRunComplete yes ; echo -e ' Relogin to ensure changed variables are active or type: "\e[0;36msource ~/.bash_profile\e[0m".' ; }
